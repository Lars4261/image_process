import math
import cv2
from PIL import Image, ImageFilter
from matplotlib import pyplot as plt
import numpy as np

img = cv2.imread('./s1086010.jpg', cv2.IMREAD_GRAYSCALE)#讀圖
w, h = img.shape[1::-1]#讀長寬
print(w,h)
cv2.imshow('My Image', img)

#padding
padding_img = np.zeros([h+2,w+2,1],dtype=np.uint8)#新建一個新的圖片
padding_img.fill(0)#填入黑色
for x in range(2, h):
    for y in range(2, w):
        padding_img[x,y] =img[x-2,y-2]#把圖放入
cv2.imshow('padding', padding_img)
#cv2.imwrite('padding_img.png', padding_img)#outputImg
paddw,paddh=padding_img.shape[1::-1]#讀長寬
print(paddw,paddh)

#GX GY
GX_img = np.zeros([paddh,paddw,1],dtype=np.uint8)#新建一個GX的圖片
GX_img.fill(0)
GY_img = np.zeros([paddh,paddw,1],dtype=np.uint8)#新建一個GY的圖片
GY_img.fill(0)

#sobel Filter
for x in range(1, h-1):
    for y in range(1, w-1):
        GX_img[x,y] =abs((1 * img[x + 1, y - 1] +2 * img[x + 1, y]+1*img[x+1,y+1])-\
                     (1*img[x-1,y-1]+2 * img[x - 1, y]+1*img[x-1,y+1]))

        GY_img[x,y] =abs((1 * img[x - 1, y - 1] +2 * img[x , y-1]+1*img[x+1,y-1])-\
                     (1*img[x-1,y+1]+2 * img[x, y+1]+1*img[x+1,y+1]))

cv2.imshow('GX_img', GX_img)
cv2.imshow('GY_img', GY_img)

#將兩張圖融合
Sobel_img = np.zeros([paddh,paddw,1],dtype=np.uint8)#新建一個新的圖片
Sobel_img.fill(0)
for x in range(1, h-1):
    for y in range(1, w-1):
        Sobel_img[x,y] =0.5*GX_img[x,y]+0.5*GY_img[x,y]#0.5只是權重值

cv2.imshow('Sobel_img', Sobel_img)

cv2.waitKey(0)
cv2.destroyAllWindows()

