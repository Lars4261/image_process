import cv2
import numpy as np
from pathlib import Path
from matplotlib import pyplot as plt
import math


def histogram_gray_value(input_img)->list:

    w, h = img.shape[1::-1]
    dict_of_count = {i: 0 for i in range(256)}

    for y in range(h):
        for x in range(w):
            dict_of_count[img[y][x]] += 1

    return dict_of_count


def show_histogram(input_dict_of_count: dict):
    label_x = [i for i in range(256)]
    label_y = [input_dict_of_count[i] for i in range(256)]
    #print(label_x)
    #print(label_y)
    fig = plt.figure('s1086010_Histogram', figsize=(5, 5))
    plt.bar(label_x, label_y)
    plt.xlabel("Gray value")
    plt.ylabel("Count")




if __name__ == "__main__":
    #img = cv2.imread('./s1086010.jpg', cv2.IMREAD_GRAYSCALE)
    img = cv2.imread('./i022.jpg', cv2.IMREAD_GRAYSCALE)

    w, h = img.shape[1::-1]
    content_w = 50
    img = cv2.resize(img, (content_w, int((content_w *h)/w)))

    w, h = img.shape[1::-1]
    print(w,h)
    subimage_w = int(w / 4)
    subimage_h = int(h / 4)
    #cv2.rectangle(img, (0, 0), (subimage_w, subimage_h), (255, 255, 255), 1)
    cv2.imshow('mini img', img)
    #cv2.waitKey(0)
    #plt.show()

    sub_image_histogram=[]
    sub_xy = []
    for x in range(w-subimage_w):
        for y in range(h-subimage_h):
            img_mark = cv2.rectangle(np.ndarray.copy(img)  , (x, y), (x+subimage_w, y+subimage_h), (255, 255, 255), 1)
            cv2.imshow('My Image-2', img_mark)

            sub_image = img[y:y+subimage_h,x:x+subimage_w]
            #cv2.imshow('My Image-sub', sub_image)
            sub_image_histogram.append(histogram_gray_value(sub_image))
            sub_xy.append((y,x))
            cv2.waitKey(1)


    global_histogram = list(histogram_gray_value(img).values())


    MSE_list = []

    for sub_historgram in sub_image_histogram:

        a = np.array(list(sub_historgram.values()))
        b = np.array(global_histogram)
        #print(a-b)
        MSE_list.append( np.sum(np.square(a-b)) )
    #print("cccccccccccc=",MSE_list)
    print( min(MSE_list) )
    print(sub_xy[MSE_list.index(min(MSE_list))])
    x = sub_xy[MSE_list.index(min(MSE_list))][1]
    y = sub_xy[MSE_list.index(min(MSE_list))][0]
    img_mark = cv2.rectangle(np.ndarray.copy(img), (x, y), (x + subimage_w, y + subimage_h), (255, 255, 255), 1)
    cv2.imshow("YA",img_mark)
    cv2.waitKey(0)