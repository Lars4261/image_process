
import cv2
from matplotlib import pyplot as plt
import numpy as np


img = cv2.imread('./s1086010.jpg', cv2.IMREAD_GRAYSCALE)

cv2.imshow('My Image', img)
type(img)

w, h = img.shape[1::-1]
"""
for x in range(w):
    for y in range(h):
        print(img[y][x])
"""

dictOfCount ={i:0 for i in range(256)}

for y in range(h):
    for x in range(w):
        dictOfCount[img[y][x]] += 1
print(dictOfCount)


#plt.imshow(img)

labelX=[i for i in range(256)]
labelY=[dictOfCount[i] for i in range(256)]
print(labelX)
print(labelY)
fig = plt.figure('s1086010_Histogram', figsize=(5, 5))
plt.bar(labelX,labelY)
plt.xlabel("Gray value")
plt.ylabel("Count")

plt.show()
cv2.waitKey(0)





